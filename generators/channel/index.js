'use strict'

const Generator = require('yeoman-generator')
const chalk = require('chalk')
const yosay = require('yosay')
const async = require('async')

module.exports = class extends Generator {
  prompting () {
    if (!this.options.env.options.namespace) {
      this.log(yosay(`Welcome to the premium ${chalk.red('reekoh-node:channel')} generator!`))
    }
  }

  writing () {
    async.waterfall([
      (done) => {
        this.fs.copy(
          this.templatePath('.'),
          this.destinationPath('.'))

        this.fs.copy(
          this.templatePath('.dockerignore'),
          this.destinationPath('.dockerignore'))

        this.fs.copy(
          this.templatePath('.npmignore'),
          this.destinationPath('.gitignore'))

        this.fs.copy(
          this.templatePath('.gitlab-ci.yml'),
          this.destinationPath('.gitlab-ci.yml'))

        this.fs.copy(
          this.templatePath('.jshintrc'),
          this.destinationPath('.jshintrc'))

        done()
      }
    ], (err) => {
      if (err) return console.log(err)
      this.installDependencies({bower: false})
    })
  }
}

'use strict'

const chalk = require('chalk')
const yosay = require('yosay')
const Generator = require('yeoman-generator')

const APP_NAME = 'reekoh-node'

const PLUGIN_SEEDS = [
  'Channel',
  'Connector',
  'Exception Logger',
  'Gateway',
  'Inventory Sync',
  'Logger',
  'Service',
  'Storage',
  'Stream'
]

module.exports = class extends Generator {
  initializing () {
    let self = this
    let seedNames = []

    PLUGIN_SEEDS.forEach(seed => {
      seedNames.push(seed.replace(/\s+/g, '-').toLocaleLowerCase())
    })

    self.prompt([{
      type: 'list',
      name: 'seed',
      message: 'Select plugin seed to install:',
      choices: PLUGIN_SEEDS
    }]).then(function (answers) {
      let selectedSeed = answers.seed.replace(/\s+/g, '-').toLocaleLowerCase()
      
      console.log(`Generating seed project for '${chalk.green(selectedSeed)}' plugin..`)

      if (seedNames.includes(selectedSeed)) {
        self.composeWith(`${APP_NAME}:${selectedSeed}`, {local: require.resolve(`../${selectedSeed}`)})
      } else {
        console.log('Cant find the generator for the selected plugin seed.')
      }
    })
  }
}

'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Service()
let serviceClient = null

/**
 * Emitted when device data is received.
 * @param {object} data The data coming from the device represented as JSON Object.
 */
plugin.on('data', (data) => {
  /*
   * Send the data to the service and expect a result.
   *
   * Example:
   *
   * serviceClient.send(data, (error, result) => {
   *     if (error) {
   *         plugin.logException(error)
   *     }
   *     else {
   *         plugin.pipe(data, result)
   *          .then(() => {
   *            plugin.log({
   *              title: 'Data sent to service client.',
   *              data: data
   *            })
   *          .catch((err) => {0.
   *            plugin.logException(err)
   *          })
   *     }
   * })
   */
  console.log(data)
})

/**
 * Emitted when the platform bootstraps the plugin. The plugin should listen once and execute its init process.
 */
plugin.once('ready', () => {
  /*
   *  Initialize the connection using the plugin.config. See config.json
   *  You can customize config.json based on the needs of your plugin.
   *  Reekoh will inject these configuration parameters as plugin.config when the platform bootstraps the plugin.
   *
   *  Example:
   *
   * let clientId = plugin.config.clientid
   * let clientSecret = plugin.config.clientsecret
   *
   * serviceClient = new ServiceClient(clientId, clientSecret);
   *
   * Note: Config Names are based on what you specify on the config.json.
   */
  console.log(plugin.config)
  plugin.log('Service has been initialized.')
})

process.on('SIGINT', () => {
  // Do graceful exit
  serviceClient.close()
})

module.exports = plugin